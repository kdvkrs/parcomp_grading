# Parallel Computing Grading Script (`grade.py`)

**`grade.py` is the predecessor to the [`worksheet-grading` package](https://pypi.org/project/worksheet-grading) which improves on `grade.py`'s functionality and has been moved to its [own repository](https://gitlab.com/kdvkrs/worksheet_grading). This repository is thus archived and no longer maintained.**

This script aims to simplify the grading process for exercise sheet grading which is structured
in exercises with distinct sub-exercises (or "answers", if you will).

## Recommended usage

To be able to easily update the grading and merge scripts, it is recommended to pull this repository into a single directory and add the scripts to the `$PATH` variable or use an `alias` in the local shell config file. Both `grade.py` and `split.py` will operate based on the files in the directory they are run in, so the central repository can be reused for grading different sheets.

## Supported platforms

Given a Python 3 installation with the required packages, the script is
supported on the following platforms:

- Linux
- macOS
- Windows (_WSL only_, see next section)

### WSL dependencies

The "Open PDF" functionality in WSL requires either `wslview` or `xdg-open`
(package `xdg-utils`) to be installed. "Open PDF (Browser)" is not supported on
Windows Systems.

Note that Windows without WSL is _not supported_ due to dependency
incompatibilities (notably `readchar`). 

## Installing required packages

The grading script requires some external python packages which are listed in `requirements.txt`, these
can be installed using

```bash
$ pip install -r requirements.txt
```

## Configuration

The exercise sheet that is to be graded can be configured with a json file with the following keys:
- `sheetname` (required): used to specify the sheet name (i.e. prefix of the used CSV files)
- `exercises` (required): points of the exercises on the sheet as key-value pairs (values are arrays of sub-exercise points). Note that exercise keys must be _strings_ (as they would otherwise violate JSON syntax) and sub-exercise points must be integer values.
- `group-size` (optional): specifies the (maximum) number of students per group, is autodetected from the CSV files if not specified
- `pdf-path` (optional): specifies a custom path for the submission pdf files (default is `pdf/`)

```json
{
        "sheetname":"blatt2",
        "exercises": {
                "1": [1,1,1],
                "2": [2],
                "3": [2],
                "4": [1,6,1],
                "5": [2,1,1],
                "6": [2,2,2],
                "7": [2,5,5],
                "8": [7,3]
        },
        "group-size": 2
}
```

In the above example, the `sheetname` is `blatt2` and exercise 4 consists of 3 sub-exercises (answers) with 1, 6 and 1
points, respectively.

Another example `config.json` is contained in this repository.

## Requirements

The script takes one (optional) argument `-e` that specifies the range of
exercises that should be graded in the format `FIRSTEX-LASTEX` (e.g. `-e
1-4`). *For every exercise*, there must be a corresponding CSV file named
`SHEETNAME_EXNUM.csv`, where `SHEETNAME` is the sheet name set in the
configuration section and `EXNUM` a two-digit decimal number for the exercise
number (for example, if `SHEETNAME=blatt1` and the file describes exercise 1,
the file should be called `blatt1_01.csv`).

Using `-s`, a suffix for the csv files can be used, such that only csv files with this suffix are considered (
i.e. `-s _tutor1` takes only files suffixed with `_tutor1`, e.g. `blatt1_01_tutor1.csv`).

These CSV files are expected to have the following format:

```csv
"exercise","group","student_1","student_2","points","feedback"
1,"Group 1","Frank Zappa",NA,NA,""
1,"Group 2","Ruth Underwood","George Duke",3,"@1@0@correct
@2@0@correct
@3@-0.5@j should be private"
```

Note that quotes are only required if the column spans over multiple rows.
CSV files should also contain the same "keys", i.e. student and group names.
NA marks unassigned student_2 and points fields. The feedback field accepts 
a string matching the regular expression `@([0-9]+)@(-?[0-9]*(\.[0-9]+)?)@(.*)`.
The first number is the sub-exercise number, the second one the number of points that are deducted. 

The script also supports opening PDF files for each group, these have to be contained in
a folder `pdf/` and must be named `GROUPNAME.pdf`, where `GROUPNAME` is the name of
the group the PDF was submitted by (PDF's can be exported in this format from Moodle/TUWEL).

You can specify a custom configuration file path using the `-c` argument.

For accessibility and compatibility purposes, the colored terminal output can be turned off using
the `--no-color` argument.

## Saving and exporting

The script exports the same file type that can be read, using the `Write to csv [w]` feature in
the main menu. Furthermore, after grading of every exercise and on exit with `Ctrl+C` (which is
possible in any menu prompt) autosaves will be created for common deductions and the currently
assigned grades (files `.SHEETNAME_autosave.csv` and `.sheetname_deductions.csv`, respectively).

## Export to grading sheet

`grade.py` supports exporting the grades to a Moodle grading worksheet. For
this, execute the script with the `-gf GRADING_WORKSHEET` argument, where
`GRADING_WORKSHEET` is the path of the grading worksheet you want to export the
grades to. Grades and grade timestamps ("last modified (grade)") for students
who did not make a submission are left empty. For all other students, the
"last modified (grade)" field will be set to the current time, which is retrieved
using `datetime.now()`, i.e., using the current system timestamp.

```bash
$ ./grade.py -gf GRADING_WORKSHEET
```

The script will report whether exporting the grades was successful and the
grades are written to a newly created file called `graded-GRADING_WORKSHEET`, where
`GRADING_WORKSHEET` is again the path of the input grading file.

## Limitations

The following limitations apply in the current version (if needed, this can be addressed):

- Only subsequent exercises can be selected, i.e. there must be a
corresponding CSV file for each element in the selected exercise range
- There is a maximum of 15 Exercises per sheet (such that they can be selected with a hex digit from the group view)

# Split and Merge Script (`split.py`)

The split script can be used to split the exercise csv files into multiple files with given suffixes and merge them
together again. This is intended as a supplement to the `grade.py` script to be able to more easily split execises upon
several tutors.

For example, a file called `blatt1_02.csv` can be split into two files `blatt1_02_tutor1.csv` and `blatt1_02_tutor2.csv`
using

```bash
$ ./split.py blatt1_02.csv _tutor1 _tutor2
```

The resulting two files can be merged together to obtain the original file using

```bash
$ ./split.py --merge blatt1_02.csv _tutor1 _tutor2
```

Files are split on a line-by-line basis, e.g. if there are 12 groups with subsequent numbering that should be split upon
3 tutors, groups `1,4,7,10` will be assigned to tutor 1, `2,5,8,11` to tutor 2, and `3,6,9,12` to tutor three.
