#!/usr/bin/env python

# standard library imports
import csv
import argparse
import re
import os
import sys
import webbrowser  # for opening web browser to display pdfs
import subprocess  # for opening external pdf viewers
import json  # json config file parsing
import readline  # overwrites input() for arrow key navigation
import datetime as dt
import statistics

# external imports
import readchar  # used for menu navigation
import colorama  # cross-platform terminal colors
import pandas as pd # easier csv file modification 

# ========== LICENSE AND AUTHOR ========== #

__author__ = "Klaus Kraßnitzer"
__copyright__ = "Copyright (C) 2022 " + __author__
__version__ = "1.2"
__license__ = "Distributed under the MIT License: <https://mit-license.org/>"
__program__ = "Parallel Computing Grading Script"
__repository__ = "<https://gitlab.com/kdvkrs/parcomp_grading>"


def print_version():
    print(bright(light_blue(" ".join([__program__, __version__]))))
    print(bright(__copyright__))
    print(__license__)
    print()
    print("Written by {}, see".format(__author__))
    print(__repository__)


# ========== CONSTANTS ========== #

EX_PATTERN = r"([0-9]+)-([0-9]+)"
CSV_BASENAME = "_{:02d}.csv"
CSV_PATTERN = r"_([0-9][0-9]).csv"
AUTOSAVE_FILENAME = ".{}_autosave.csv"
DEDUCTION_FILE = ".{}_deductions.csv"

# format for student header in csv files
STUDENT_PATTERN = r"student_([1-9])"
STUDENT_FORMAT = "student_{}"

NA = "NA"

FEEDBACK_PATTERN = r"@([0-9]+)@(-?[0-9]*(\.[0-9]+)?)@(.*)"

CSV_FIRSTLINE = "exercise,group,student_1,student_2,points,feedback\n"
DEDUCTIONS_FIRSTLINE = "ex_num,ans_num,reason,points,count\n"

DEFAULT_FIRSTEX = 1
DEFAULT_LASTEX = 15

GRADED = 0
PARTIALLY_GRADED = 1
UNGRADED = 2

# alignment widths
MAX_GROUP_LEN = 0
MAX_NAMES_LEN = 0

# symbols
RIGHT_ARROW = "→"
LEFT_ARROW = "←"
UP_ARROW = "↑"
DOWN_ARROW = "↓"
ENTER_ARROW = "↲"

PDF_VIEWERS = ["evince", "okular", "xdg-open", "open", "wslview"]

# ========== JSON CONFIGURATION ========== #

# number of students per group
GROUP_SIZE = None

# default path of submission pdfs
PDF_PATH = "pdf/"

# exercises and sheet name
EXERCISES = {}
SHEETNAME = ""

# whether color is enabled
COLOR = True

def init_sheet():
    sh = Sheet()

    for i in range(DEFAULT_FIRSTEX, DEFAULT_LASTEX + 1):
        exc = EXERCISES.get(str(i))
        if exc is not None:
            sh.exercises.append(Exercise(i, len(exc), sum(exc)))

    return sh


def detect_group_size(df: pd.DataFrame):
    global GROUP_SIZE
    if GROUP_SIZE is None:
        print_info("Trying to detect group size from CSV files") 
        i = 1
        while STUDENT_FORMAT.format(i) in df:
            i += 1
        i -= 1
        if i == 0:
            print_err("No valid student columns detected in CSV files, exiting")
            exit(1)
        else:
            print_info(f"Detected {i} students per group")
            GROUP_SIZE = i


# ========== TERMINAL COLOR ========== #

def red(msg):
    if COLOR:
        return colorama.Fore.RED + str(msg) + colorama.Fore.RESET
    else:
        return str(msg)


def green(msg):
    if COLOR:
        return colorama.Fore.GREEN + str(msg) + colorama.Fore.RESET
    else:
        return str(msg)


def magenta(msg):
    if COLOR:
        return colorama.Fore.MAGENTA + str(msg) + colorama.Fore.RESET
    else:
        return str(msg)


def yellow(msg):
    if COLOR:
        return colorama.Fore.YELLOW + str(msg) + colorama.Fore.RESET
    else:
        return str(msg)


def light_red(msg):
    if COLOR:
        return colorama.Fore.LIGHTRED_EX + str(msg) + colorama.Fore.RESET
    else:
        return str(msg)


def light_yellow(msg):
    if COLOR:
        return colorama.Fore.LIGHTYELLOW_EX + str(msg) + colorama.Fore.RESET
    else:
        return str(msg)


def light_green(msg):
    if COLOR:
        return colorama.Fore.LIGHTGREEN_EX + str(msg) + colorama.Fore.RESET
    else:
        return str(msg)


def light_blue(msg):
    if COLOR:
        return colorama.Fore.LIGHTBLUE_EX + str(msg) + colorama.Fore.RESET
    else:
        return str(msg)


def dim(msg):
    return colorama.Style.DIM + str(msg) + colorama.Style.RESET_ALL


def bright(msg):
    return colorama.Style.BRIGHT + str(msg) + colorama.Style.RESET_ALL


def highlight(msg):
    return colorama.Back.LIGHTBLACK_EX + bright(str(msg)) + colorama.Back.RESET


# ========== DATA STRUCTURE ========== #

class Exercise:
    def __init__(self, ex_num, num_answ, max_points):
        self.ex_num = ex_num
        self.answers = []
        self.num_answ = num_answ
        for i in range(0, num_answ):
            x = Answer(i + 1)
            self.answers.append(x)
        self.max_points = max_points

    def deduct(self, answer_num, reason, points):
        if answer_num > self.num_answ:
            print_err("Invalid Answer")
            return
        self.answers[answer_num - 1].deduct(reason, points)

    def _is_graded(self):
        ans_status = []
        for a in self.answers:
            ans_status.append(a.graded)
        if True not in ans_status:
            return UNGRADED
        else:
            if False not in ans_status:
                return GRADED
            else:
                return PARTIALLY_GRADED

    def mark_correct(self, ans_num):
        self.answers[ans_num - 1].mark_correct()

    def _calc_points(self):
        p = self.max_points
        for a in self.answers:
            p += a.deductions.deducted_points()
        if p < 0:
            p = 0
        # don't format as float if not necessary
        if p == round(p):
            p = int(p)
        return p

    graded = property(_is_graded)
    act_points = property(_calc_points)

    def csv_feedback(self):
        if self.graded == GRADED:
            fb_str = ""
            for ans in self.answers:
                fb_str += ans.csv_feedback_string()
            # remove last newline
            if fb_str != "":
                fb_str = fb_str[:-1]
            return fb_str
        else:
            return NA
     
    def csv_points(self):
        if self.graded == GRADED:
            points = self.act_points
        else:
            points = NA
        return points

    def short_rep(self):
        return "Exercise {} ({})".format(self.ex_num, self._grade_string())

    def _grade_string(self):
        grade_string = "not graded"
        if self.graded == GRADED:
            grade_string = "{}/{} Points".format(self.act_points, self.max_points)
        elif self.graded == PARTIALLY_GRADED:
            grade_string = "partially graded"
        return grade_string

    def long_rep(self):
        rep = "Exercise {} ({})\n\n".format(self.ex_num, self._grade_string())
        for a in self.answers:
            rep += a.rep(self.ex_num, key=True)
        return rep

    def html_string(self):
        s = "<p>\n<b>Exercise {} (max {}P): {}P</b><ul>".format(self.ex_num, self.max_points, self.act_points)
        for answer in self.answers:
            s += answer.html_string(self.ex_num) + "\n"
        s += "</ul></p>"
        return s


class Answer:
    def __init__(self, ans_num):
        self.num = ans_num
        self.deductions = Deductions(ans_num)
        self.graded = False

    def deduct(self, reason, points):
        self.deductions.add(reason, points)
        self.graded = True

    def mark_correct(self):
        self.deductions.clear()
        self.graded = True

    def html_string(self, ex_num):
        return "<li>{}.{}: {}</li>".format(ex_num, self.num, self.deductions.html_string())

    def feedback_string(self):
        return self.deductions.feedback_string()

    def csv_feedback_string(self):
        return self.deductions.csv_feedback_string()

    def rep(self, ex_num, key=False):
        repstr = ""
        if key:
            repstr += "\t[{}] ".format(magenta(self.num))
        grade_string = "not graded"
        if self.graded:
            grade_string = self.deductions.rep()
        repstr += "{}.{}: {}\n".format(ex_num, self.num, grade_string)
        return repstr


class Sheet:
    def __init__(self):
        self.exercises = []

    def html_string(self):
        s = ""
        for e in self.exercises:
            if e.max_points > 0:
                s += e.html_string() + "\n"
        max_points = 0
        reached_points = 0
        for e in self.exercises:
            max_points += e.max_points
            reached_points += e.act_points
        s += "<p><b>Total (max {}P): {}P</b></p>\n".format(max_points, reached_points)
        return s

    def print_html(self):
        print(self.html_string())

    def deduct(self, ex_num, answer_num, grade_text, deduction):
        self.exercises[ex_num - 1].deduct(answer_num, grade_text, deduction)

    def mark_correct(self, ex_num, ans_num):
        self.exercises[ex_num - 1].mark_correct(ans_num)

    def range_points(self, ex_range=None):
        max_points = 0
        act_points = 0
        for e in self.exercises:
            if ex_range is None or e.ex_num in ex_range:
                max_points += e.max_points
                act_points += e.act_points
        return act_points, max_points

    def grade_status(self, ex_range=None):
        ex_status = []
        for e in self.exercises:
            if ex_range is None or e.ex_num in ex_range:
                ex_status.append(e.graded)
        if not (GRADED in ex_status or PARTIALLY_GRADED in ex_status):
            return UNGRADED
        else:
            if not (PARTIALLY_GRADED in ex_status or UNGRADED in ex_status):
                return GRADED
            else:
                return PARTIALLY_GRADED

    def filter_exercises(self, first_ex, last_ex):
        new_exs = []
        for e in self.exercises:
            if first_ex <= e.ex_num <= last_ex:
                new_exs.append(e)
        self.exercises = new_exs


class Deductions:
    def __init__(self, ans_num):
        self.deds = []
        self.ans_num = ans_num

    def add(self, reason, points):
        self.deds.append((reason, points))

    def feedback_string(self):
        s = ""
        if len(self.deds) == 0:
            s += "-{}: correct\n".format(self.ans_num)
        else:
            for d in self.deds:
                if d[1] == 0:
                    s += "-{}: {}\n".format(self.ans_num, d[0])
                else:
                    s += "-{}: {} ({:.2f}P)\n".format(self.ans_num, d[0], d[1])
            s = s[:-1]
        return s

    def csv_feedback_string(self):
        s = ""
        if len(self.deds) == 0:
            s += "@{}@0@correct\n".format(self.ans_num)
        else:
            for d in self.deds:
                s += "@{}@{:.2f}@{}\n".format(self.ans_num, d[1], d[0])
        return s

    def html_string(self):
        if len(self.deds) == 0:
            return "correct"
        elif len(self.deds) == 1:
            if self.deds[0][1] == 0:
                return self.deds[0][0]
            else:
                return "{} ({:.2f}P)".format(self.deds[0][0], self.deds[0][1])
        else:
            s = "<ul>\n"
            for d in self.deds:
                if d[1] == 0:
                    s += "<li>{}</li>".format(d[0])
                else:
                    s += "<li>{} ({:.2f}P)</li>\n".format(d[0], d[1])
            s += "</ul>"
            return s

    def rep(self):
        if len(self.deds) == 0:
            return "correct"
        elif len(self.deds) == 1:
            if self.deds[0][1] == 0:
                return self.deds[0][0]
            else:
                return "{} ({:.2f}P)".format(self.deds[0][0], self.deds[0][1])
        else:
            s = "\n"
            for d in self.deds:
                if d[1] == 0:
                    s += "\t\t- {}\n".format(d[0])
                else:
                    s += "\t\t- {} ({:.2f}P)\n".format(d[0], d[1])
            s = s[:-1]
            return s

    def deducted_points(self):
        p = 0
        for d in self.deds:
            p += float(d[1])
        return p

    def clear(self):
        self.deds.clear()


class FrameEntry:
    def __init__(self, group, students):
        self.group = group
        self.students = students
        self.sheet = init_sheet()

    def parse_feedback(self, deds, ex_num, points, feedback):
        if points != NA:
            for fb in feedback.split("\n"):
                ln = re.finditer(FEEDBACK_PATTERN, fb)
                for m in ln:
                    ans_num = m[1]
                    deduction = m[2]
                    grade_text = m[4]
                    if float(deduction) == 0 and re.match(r"\s*correct\s*", grade_text):
                        self.sheet.mark_correct(int(ex_num), int(ans_num))
                    else:
                        self.sheet.deduct(int(ex_num), int(ans_num), grade_text, float(deduction))
                        deds.add_entry(int(ex_num), int(ans_num), grade_text, float(deduction))

    def name_len(self):
        # +2 for brackets
        return len(", ".join(self.students))+2

    def group_len(self):
        return len(self.group)

    def group_string(self, state):
        student_str = ", ".join(self.students)
        student_str = "(" + student_str + ")"
        ex_range = range(state.frame.first_ex, state.frame.last_ex + 1)
        grade_status = self.sheet.grade_status(ex_range)
        if grade_status == GRADED:
            act_points, max_points = self.sheet.range_points(ex_range)
            grade_str = light_green("{}/{} Points".format(act_points, max_points))
        elif grade_status == PARTIALLY_GRADED:
            grade_str = light_yellow("partially graded")
        elif grade_status == UNGRADED:
            grade_str = light_red("ungraded")
        else:
            raise RuntimeError("invalid grade status observed")
        return "{0:<{group_len}} {1:<{name_len}} | {2}".format(self.group, student_str, grade_str, group_len=MAX_GROUP_LEN,
                                                               name_len=MAX_NAMES_LEN)


class Frame:
    def __init__(self, first_ex, last_ex):
        self.first_ex = int(first_ex)
        self.last_ex = int(last_ex)

        # get specified csv files
        potential_files = [CSV_BASENAME.format(i) for i in range(int(first_ex), int(last_ex) + 1)]
        d = os.listdir()
        files = list(set(potential_files) & set(d))

        # revise firstex and lastex based on available files
        exs = []
        for f in files:
            m = re.search(CSV_PATTERN, f)
            exs.append(int(m.group(1)))

        if len(exs) == 0:
            print_err("No matching csv files for specified exercises and suffix found, exiting")
            exit(1)

        if min(exs) != self.first_ex:
            print_info(
                "First specified exercise ({}) is smaller than minimum csv file found ({}), setting first "
                "exercise to {}".format(
                    self.first_ex, min(exs), min(exs)))
            self.first_ex = min(exs)
        if max(exs) != self.last_ex:
            print_info(
                "Last specified exercise ({}) is larger than maximum csv file found ({}), setting last exercise "
                "to {}".format(
                    self.last_ex, max(exs), max(exs)))
            self.last_ex = max(exs)

        # check if deductions file exists and create if not present
        if not os.path.isfile(DEDUCTION_FILE):
            print_info("No deduction file found, creating new one")
            with open(DEDUCTION_FILE, "w") as f:
                f.write(DEDUCTIONS_FIRSTLINE)

        self.deds = DeductionCollection()

        self.data = {}

        if AUTOSAVE_FILENAME in os.listdir():
            print_info("Restoring from autosave, delete \"{}\" if you want to start from scratch".format(
                AUTOSAVE_FILENAME))
            as_df = pd.read_csv(AUTOSAVE_FILENAME)
            as_df.fillna(NA, inplace=True)
            detect_group_size(as_df)
            for i, r in as_df.iterrows():
                group_name = r["group"]
                students = []
                for i in range(1,GROUP_SIZE+1):
                    s = r[STUDENT_FORMAT.format(i)]
                    if s != NA:
                         students.append(s)
                if group_name not in self.data.keys():
                    self.data[group_name] = FrameEntry(group_name, students)
                self.data[group_name].parse_feedback(self.deds, r["exercise"], r["points"], r["feedback"])
            self.csv_df = as_df

        else:
            print_info("No autosave found, starting from scratch.\n")
            # create data structure from first file
            df = pd.read_csv(files[0])
            df.fillna(NA, inplace=True)
            detect_group_size(df)
            for i, r in df.iterrows():
                group_name = r["group"]
                students = []
                for i in range(1,GROUP_SIZE+1):
                    s = r[STUDENT_FORMAT.format(i)]
                    if s != NA:
                        students.append(s)
                self.data[group_name] = FrameEntry(group_name, students)
                self.data[group_name].parse_feedback(self.deds, r["exercise"], r["points"], r["feedback"])
            self.csv_df = df

            # read other files
            for f in files[1::]:
                df = pd.read_csv(f)
                df.fillna(NA, inplace=True)
                for i, r in df.iterrows():
                    group_name = r["group"]
                    students = []
                    self.data[group_name].parse_feedback(self.deds, r["exercise"], r["points"], r["feedback"])
                self.csv_df = pd.concat([self.csv_df, df]) 
            self.csv_df.sort_values(["exercise", "group"], inplace=True)
                

    def update_feedback(self):
        self.csv_df["feedback"] = [self.data[r["group"]].sheet.exercises[int(r["exercise"])-1].csv_feedback() for _,r in self.csv_df.iterrows()]
        self.csv_df["points"] = [self.data[r["group"]].sheet.exercises[int(r["exercise"])-1].csv_points() for _,r in self.csv_df.iterrows()]


    def write_csv_files(self):
        self.update_feedback()
        basename = CSV_BASENAME

        for e in range(self.first_ex, self.last_ex + 1):
            # rename old file
            os.rename(basename.format(e), basename.format(e) + ".old")

            # save exercise files
            cf = self.csv_df[self.csv_df["exercise"] == e]
            cf.to_csv(basename.format(e), index=None)


    def autosave(self):
        self.update_feedback()

        self.csv_df.to_csv(AUTOSAVE_FILENAME, index=None)
        self.deds.write_csv()

    def ungraded_groups(self):
        return list(filter(lambda e: e.sheet.grade_status(ex_range=range(self.first_ex, self.last_ex + 1)) == UNGRADED,
                           self.data.values()))

    def partial_groups(self):
        return list(
            filter(lambda e: e.sheet.grade_status(ex_range=range(self.first_ex, self.last_ex + 1)) == PARTIALLY_GRADED,
                   self.data.values()))

    def graded_groups(self):
        return list(filter(lambda e: e.sheet.grade_status(ex_range=range(self.first_ex, self.last_ex + 1)) == GRADED,
                           self.data.values()))


class DeductionCollection:
    def __init__(self):
        self.entries = []
        with open(DEDUCTION_FILE, "r") as f:
            reader = csv.reader(f, delimiter=",")
            for (i, line) in enumerate(reader):
                if i != 0:
                    ex_num = line[0]
                    ans_num = line[1]
                    reason = line[2]
                    points = line[3]
                    count = line[4]
                    self.new_entry(ex_num, ans_num, reason, points, count)

    def add_entry(self, ex_num, ans_num, reason, points):
        found = False
        for e in self.entries:
            if e.ex_num == ex_num and e.ans_num == ans_num and \
                    e.reason == reason and e.points == points:
                #e.increment_count()
                found = True
                break
        if not found:
            self.new_entry(ex_num, ans_num, reason, points, 1)

    def new_entry(self, ex_num, ans_num, reason, points, count):
        self.entries.append(DeductionEntry(ex_num, ans_num, reason, points, count))

    def write_csv(self):
        with open(DEDUCTION_FILE, "w") as f:
            f.write(DEDUCTIONS_FIRSTLINE)
            for d in self.entries:
                f.write(d.csv_string())

    def get(self, ex_num, ans_num):
        deds = list(filter(lambda e: e.ex_num == ex_num and e.ans_num == ans_num, self.entries))
        deds.sort(key=lambda e: e.count, reverse=True)
        return deds

    def __str__(self):
        s = ""
        for d in self.entries:
            s += str(d) + "\n"
        return s


class DeductionEntry:
    def __init__(self, ex_num, ans_num, reason, points, count):
        self.ex_num = int(ex_num)
        self.ans_num = int(ans_num)
        self.reason = reason
        self.points = float(points)
        self.count = int(count)

    def increment_count(self):
        self.count += 1

    def csv_string(self):
        return "{},{},\"{}\",{},{}\n".format(self.ex_num, self.ans_num, self.reason, self.points, self.count)

    def __str__(self):
        return "{}.{}: {} ({} Points)".format(self.ex_num, self.ans_num, self.reason, self.points)


# ========== COMMAND LINE LOGIC ========== #

class State:
    def __init__(self, frame):
        self.frame = frame


def group_overview(frame):
    ungraded_groups = frame.ungraded_groups()
    graded_groups = frame.graded_groups()
    partial_groups = frame.partial_groups()

    strings = ["Graded:", "Partially Graded:", "Ungraded:"]
    max_len = max(map(len, strings))

    return "Found {0} groups: \n\t{1:<{max_len}} {2:>{max_len}}\n\t" \
           "{3:<{max_len}} {4:>{max_len}}\n\t{5:<{max_len}} {6:>{max_len}}\n" \
        .format(
            bright(len(frame.data.values())),
            strings[0],
            light_green(len(graded_groups)),
            strings[1],
            light_yellow(len(partial_groups)),
            strings[2],
            light_red(len(ungraded_groups)),
            max_len=max_len,
        )


def clear_terminal():
    print("\033c", end="")


def handle_action(state, actions, message, prompt_string=None):
    keys = []
    descriptions = []
    methods = []

    create_prompt_string = prompt_string is None

    # prepare lists and prompt
    action_format = "{} [{}]"
    if create_prompt_string:
        prompt_string = ""
    for a in actions:
        k, d, m = a
        keys.append(k)
        descriptions.append(d)
        methods.append(m)
        if create_prompt_string:
            prompt_string += action_format.format(d, magenta(k))
            prompt_string += "  "

    c = None
    while c is None:
        print(message, end="")

        print()
        print(prompt_string)
        c = readchar.readkey()

        # standard key bindings
        if c == readchar.key.CTRL_C:
            state.frame.autosave()
            exit(0)

        clear_terminal()

        matching = []
        for k in keys:
            if c in k:
                matching.append(k)
        if len(matching) > 0:
            methods[keys.index(matching[0])](state)
        else:
            ch = c if c.isalnum() else c.encode()
            print("\nUnknown option: \"{}\"\n".format(ch))
            c = None

    return c


def menu(frame):
    while True:
        message = "\n" + bright(light_blue("Parallel Computing Grading Script V{}".format(__version__))) + "\n\n"
        message += group_overview(frame)
        actions = [
            ("g", "Quick Grade", act_quick_grade),
            ("s", "Search a group", act_search_group),
            ("a", "Show all groups", act_all_groups),
            ("w", "Write to CSV files", act_write_output),
            ("c", "Check deductions", act_sanity_check),
            ("t", "Statistics", act_statistics),
            ("x", "Exit", act_exit),
        ]
        state = State(frame)
        handle_action(state, actions, message)


def select_group(state, groups):
    if len(groups) == 0:
        print(red("No matching group found\n"))
    elif len(groups) == 1:
        state.page_groups = groups
        state.selection = 0
        act_group_view(state)
        state.selection = None
        state.page_groups = None
    else:
        group_selection_view(state, groups)


def group_selection_view(state, groups):
    if not hasattr(state, "page") or state.page is None:
        state.page = 0

    actions = [
        ("1", "Select Group 1", act_sel_1),
        ("2", "Select Group 2", act_sel_2),
        ("3", "Select Group 3", act_sel_3),
        ("4", "Select Group 4", act_sel_4),
        ("5", "Select Group 5", act_sel_5),
        ("n" + readchar.key.RIGHT, "Next Page", act_next_page),
        ("p" + readchar.key.LEFT, "Previous Page", act_previous_page),
        ("b", "Back", act_back)
    ]
    while True:
        state.sel_method = act_group_view

        first_index = state.page * 5 % len(groups)
        state.page_groups = []
        message = bright("Select a group:\n\n")

        # compute max lens
        reset_max_lens()
        for i in range(5):
            index = (first_index + i) % len(groups)
            state.page_groups.append(groups[index])
            update_max_lens(groups[index].group_len(), groups[index].name_len())

        for i in range(5):
            new_group_str = "\t[{}] {}\n".format(magenta(i + 1), state.page_groups[i].group_string(state))
            message += new_group_str

        message += dim("\n\tShowing entries {}-{} of {}\n".format(first_index + 1,
                                                                  (((first_index + 4) % len(groups)) + 1), len(groups)))

        prompt_string = "Select Group [{}]  Next page [{}]  Previous page [{}]  Back [{}]" \
            .format(
                magenta("1") + "-" + magenta("5"),
                magenta("n") + ", " + magenta(RIGHT_ARROW),
                magenta("p") + ", " + magenta(LEFT_ARROW),
                magenta("b")
            )
        act = handle_action(state, actions, message, prompt_string=prompt_string)
        if act == "b":
            break

    state.page = None
    state.page_groups = None
    state.sel_method = None


# ========== ACTIONS ========== #

def act_exit(state):
    state.frame.autosave()
    exit(0)


def act_quick_grade(state):
    groups = state.frame.partial_groups()
    groups.extend(state.frame.ungraded_groups())
    if len(groups) == 0:
        print("All groups are graded\n")
    else:
        select_group(state, groups[0:1])


def act_write_output(state):
    print("Writing data to csv files and renaming existing csv files to FILENAME.old")
    state.frame.write_csv_files()
    print("Successfully wrote data to csv files")


def act_sel_1(state):
    state.selection = 0
    state.sel_method(state)
    state.selection = None


def act_sel_2(state):
    state.selection = 1
    state.sel_method(state)
    state.selection = None


def act_sel_3(state):
    state.selection = 2
    state.sel_method(state)
    state.selection = None


def act_sel_4(state):
    state.selection = 3
    state.sel_method(state)
    state.selection = None


def act_sel_5(state):
    state.selection = 4
    state.sel_method(state)
    state.selection = None


def act_sel_6(state):
    state.selection = 5
    state.sel_method(state)
    state.selection = None


def act_sel_7(state):
    state.selection = 6
    state.sel_method(state)
    state.selection = None


def act_sel_8(state):
    state.selection = 7
    state.sel_method(state)
    state.selection = None


def act_sel_9(state):
    state.selection = 8
    state.sel_method(state)
    state.selection = None


def act_sel_10(state):
    state.selection = 9
    state.sel_method(state)
    state.selection = None


def act_sel_11(state):
    state.selection = 10
    state.sel_method(state)
    state.selection = None


def act_sel_12(state):
    state.selection = 11
    state.sel_method(state)
    state.selection = None


def act_sel_13(state):
    state.selection = 12
    state.sel_method(state)
    state.selection = None


def act_sel_14(state):
    state.selection = 13
    state.sel_method(state)
    state.selection = None


def act_sel_15(state):
    state.selection = 14
    state.sel_method(state)
    state.selection = None


def act_back(_):
    # NO OPERATION
    return


def act_next_page(state):
    if not hasattr(state, "page") or state.page is None:
        state.page = 1
    else:
        state.page += 1


def act_previous_page(state):
    if not hasattr(state, "page") or state.page is None:
        state.page = 0
    else:
        state.page -= 1


def act_next_exc(state):
    if not hasattr(state, "ex_num") or state.ex_num is None:
        state.ex_num = 1
    else:
        state.ex_num += 1


def act_previous_exc(state):
    if not hasattr(state, "ex_num") or state.ex_num is None:
        state.ex_num = 0
    else:
        state.ex_num -= 1


def act_next_ans(state):
    if not hasattr(state, "ans_num") or state.ans_num is None:
        state.ans_num = 1
    else:
        state.ans_num += 1
    state.ded_page = 0


def act_previous_ans(state):
    if not hasattr(state, "ans_num") or state.ans_num is None:
        state.ans_num = 0
    else:
        state.ans_num -= 1
    state.ded_page = 0


def act_all_groups(state):
    groups = list(state.frame.data.values())
    select_group(state, groups)


def act_search_group(state):
    name = input("Enter search string: ").lower()
    clear_terminal()
    matching_groups = list(
        filter(lambda e: name in e.group.lower() or any(name in s.lower() for s in e.students),
               state.frame.data.values()))
    select_group(state, matching_groups)


def act_previous_deds(state):
    if not hasattr(state, "ded_page") or state.ded_page is None:
        state.ded_page = 0
    else:
        if state.ded_page > 0:
            state.ded_page -= 1


def act_next_deds(state):
    if not hasattr(state, "ded_page") or state.ded_page is None:
        state.ded_page = 1
    else:
        if (state.ded_page + 1) * 5 < len(state.deds):
            state.ded_page += 1


def act_open_pdf(state):
    pdf = PDF_PATH + state.selected_group.group.replace("/", "") + ".pdf"
    if not os.path.isfile(pdf):
        print("No such file: " + pdf + "\n")
    else:
        webbrowser.open_new("file://" + os.getcwd() + "/" + pdf)


def act_open_pdf_viewer(state):
    pdf = PDF_PATH + state.selected_group.group.replace("/", "") + ".pdf"
    if not os.path.isfile(pdf):
        print("No such file: " + pdf + "\n")
    else:
        for viewer in PDF_VIEWERS:
            cmd = "which " + viewer + " | grep -o " + viewer + " > /dev/null && echo '0' || echo '1'"
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            (output, err) = p.communicate()
            p.wait()
            if output == b'0\n':
                subprocess.Popen([viewer, pdf], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                return

        print("Either no supported pdf viewer or 'which' not installed, list of supported pdf viewers:")
        print(*PDF_VIEWERS, sep=", ")
        print("\n")


def act_mark_correct(state):
    state.selected_exc.answers[state.ans_num].mark_correct()
    act_next_ans(state)


def act_new_ded(state):
    print(bright("Add new deduction (cancel with EOF)"), end="\n\n")
    try:
        reason = input("Enter reason for deduction: ").replace("\"", "'")
        while True:
            points = input("Enter points that should be deducted: ")
            try:
                parsed_points = float(points)
                if parsed_points > 0:
                    parsed_points = -parsed_points
                break
            except ValueError:
                print("Invalid floating point number {}".format(points))
        state.frame.deds.add_entry(state.ex_num + 1, state.ans_num + 1, reason, parsed_points)
        state.selected_exc.deduct(state.ans_num + 1, reason, parsed_points)
        clear_terminal()
    except EOFError:
        print("EOF encountered")
        clear_terminal()


def act_ded_scroll_up(state):
    state.cur_ded = max(0, state.cur_ded-1)
    pass


def act_ded_scroll_down(state):
    state.cur_ded = min(state.cur_ded+1, len(state.selected_exc.answers[state.ans_num].deductions.deds)-1)
    pass


def act_del_ded(state):
    state.last_rem_ded.append((state.selected_exc.answers[state.ans_num].deductions.deds[state.cur_ded], state.cur_ded))
    state.selected_exc.answers[state.ans_num].deductions.deds.pop(state.cur_ded)


def act_undo_last_ded_rem(state):
    if len(state.last_rem_ded) == 0:
        print(red("Nothing to undo\n"))
        return
    dc, di = state.last_rem_ded[-1]
    state.selected_exc.answers[state.ans_num].deductions.deds.insert(di, dc)
    state.last_rem_ded.pop()


def act_rem_ded(state):
    if len(state.selected_exc.answers[state.ans_num].deductions.deds) == 0:
        print(bright(red("No deductions found to remove")), end="\n\n")
        return
    state.cur_ded = 0
    state.last_rem_ded = []
    actions = [
        (readchar.key.UP, "Scroll up", act_ded_scroll_up),
        (readchar.key.DOWN, "Scroll down", act_ded_scroll_down),
        ("r"+readchar.key.ENTER, "Remove", act_del_ded),
        ("u", "Undo", act_undo_last_ded_rem),
        ("b", "Back", act_back),
    ]
    prompt_string = "Scroll [{},{}]  Remove selected [{}]  Undo last removal [{}]  Back [{}]" \
        .format(
            magenta(UP_ARROW),
            magenta(DOWN_ARROW),
            magenta("r") + ", " + magenta(ENTER_ARROW),
            magenta("u"),
            magenta("b"),
        )


    while True:
        message = bright("Remove deductions") + "\n\n"
        for i, (ds, dp) in enumerate(state.selected_exc.answers[state.ans_num].deductions.deds):
            ded_str = f"\t- {ds} ({dp} P)\n"
            if i == state.cur_ded:
                message += highlight(ded_str)
            else:
                message += ded_str
        act = handle_action(state, actions, message, prompt_string)
        if act == "b":
            break
    
    state.frame.autosave()


def act_deduct_selected(state):
    if state.ded_page * 5 + state.selection < len(state.deds):
        ded = state.deds[state.ded_page * 5 + state.selection]
        state.selected_exc.deduct(state.ans_num + 1, ded.reason, ded.points)
        ded.increment_count()


def act_ans_view(state):
    if not state.selection < len(state.selected_exc.answers):
        print("invalid answer selection ({})\n".format(state.selection + 1))
        return

    state.ded_page = 0

    actions = [
        ("n" + readchar.key.RIGHT, "Next", act_next_ans),
        ("p" + readchar.key.LEFT, "Previous", act_previous_ans),
        ("1", "Select Deduction 1", act_sel_1),
        ("2", "Select Deduction 2", act_sel_2),
        ("3", "Select Deduction 3", act_sel_3),
        ("4", "Select Deduction 4", act_sel_4),
        ("5", "Select Deduction 5", act_sel_5),
        (readchar.key.UP, "Previous deds", act_previous_deds),
        (readchar.key.DOWN, "Next deds", act_next_deds),
        ("c", "Mark as correct", act_mark_correct),
        ("d", "Add new deduction", act_new_ded),
        ("r", "Remove deduction", act_rem_ded),
        ("b", "Back", act_back)
    ]
    prompt_string = "Mark as correct [{}]  Choose Deduction [{}]  Add new deduction [{}]  Scroll Common Deductions [{}]" \
                    "\nNext Subexercise [{}]  Previous Subexercise [{}]  Remove Deduction [{}]  Back [{}]" \
        .format(
            magenta("c"),
            magenta("1") + "-" + magenta("5"),
            magenta("d"),
            magenta(UP_ARROW) + ", " + magenta(DOWN_ARROW),
            magenta("n") + ", " + magenta(RIGHT_ARROW),
            magenta("p") + ", " + magenta(LEFT_ARROW),
            magenta("r"),
            magenta("b"),
        )

    state.ans_num = state.selection
    state.deds = state.frame.deds.get(state.ex_num + 1, state.ans_num + 1)
    while True:
        if state.ans_num + 1 > len(state.selected_exc.answers):
            if state.ex_num + 2 > state.frame.last_ex:
                return
            else:
                state.ans_num = 0
                state.ex_num += 1
                state.selected_exc = state.selected_group.sheet.exercises[state.ex_num]
                state.frame.autosave()
        if state.ans_num < 0:
            if state.ex_num < state.frame.first_ex:
                return
            else:
                state.ans_num = len(state.selected_group.sheet.exercises[state.ex_num - 1].answers) - 1
                state.ex_num -= 1
                state.selected_exc = state.selected_group.sheet.exercises[state.ex_num]
                state.frame.autosave()

        state.deds = state.frame.deds.get(state.ex_num + 1, state.ans_num + 1)
        answer = state.selected_exc.answers[state.ans_num]

        message = state.selected_group.group_string(state) + "\n" + bright(state.selected_exc.short_rep()) + "\n\n"
        message += "Subexercise " + answer.rep(state.ex_num + 1)

        state.sel_method = act_deduct_selected

        if len(state.deds) == 0:
            message += "\nNo deductions found\n"
        else:
            message += "\nCommon Deductions:\n"
            first_ded = state.ded_page * 5
            last_ded = min((state.ded_page + 1) * 5, len(state.deds))
            j = 1
            for i in range(first_ded, last_ded):
                message += "\t[{}] {} ({:.2f}P)\n".format(magenta(j), state.deds[i].reason, state.deds[i].points)
                j += 1

        act = handle_action(state, actions, message, prompt_string=prompt_string)
        if act == "b":
            break

    state.ded_page = None
    state.deds = None
    state.ans_num = None


def act_exercise_view(state):
    if not state.selection + state.frame.first_ex in range(state.frame.first_ex, state.frame.last_ex + 1):
        print(red("invalid exercise selection ({})\n".format(state.selection + 1)))
        return

    actions = [
        ("1", "Select Subexercise 1", act_sel_1),
        ("2", "Select Subexercise 2", act_sel_2),
        ("3", "Select Subexercise 3", act_sel_3),
        ("4", "Select Subexercise 4", act_sel_4),
        ("5", "Select Subexercise 5", act_sel_5),
        ("6", "Select Subexercise 6", act_sel_6),
        ("7", "Select Subexercise 7", act_sel_7),
        ("8", "Select Subexercise 8", act_sel_8),
        ("9", "Select Subexercise 9", act_sel_9),
        ("n" + readchar.key.RIGHT, "Next", act_next_exc),
        ("p" + readchar.key.LEFT, "Previous", act_previous_exc),
        ("b", "Back", act_back)
    ]
    prompt_string = "Select Subexercise [{}]  Next [{}]  Previous [{}]  Back [{}]" \
        .format(
            magenta("1") + "-" + magenta("9"),
            magenta("n") + ", " + magenta(RIGHT_ARROW),
            magenta("p") + ", " + magenta(LEFT_ARROW),
            magenta("b")
        )

    exercises = state.selected_group.sheet.exercises

    state.ex_num = state.selection + state.frame.first_ex - 1
    while True:
        if state.ex_num + 1 > state.frame.last_ex or state.ex_num + 1 < state.frame.first_ex:
            return
        exc = exercises[state.ex_num]
        message = state.selected_group.group_string(state) + "\n\n"
        message += exc.long_rep()
        state.selected_exc = exc

        state.sel_method = act_ans_view
        act = handle_action(state, actions, message, prompt_string=prompt_string)
        if act == "b":
            state.frame.autosave()
            break


def act_group_view(state):
    selected_group = state.page_groups[state.selection % len(state.page_groups)]
    reset_max_lens()
    update_max_lens(selected_group.group_len(), selected_group.name_len())
    sheet = selected_group.sheet

    state.selected_group = selected_group

    actions = [
        ("1", "Select Exercise 1", act_sel_1),
        ("2", "Select Exercise 2", act_sel_2),
        ("3", "Select Exercise 3", act_sel_3),
        ("4", "Select Exercise 4", act_sel_4),
        ("5", "Select Exercise 5", act_sel_5),
        ("6", "Select Exercise 6", act_sel_6),
        ("7", "Select Exercise 7", act_sel_7),
        ("8", "Select Exercise 8", act_sel_8),
        ("9", "Select Exercise 9", act_sel_9),
        ("A", "Select Exercise 10", act_sel_10),
        ("B", "Select Exercise 11", act_sel_11),
        ("C", "Select Exercise 12", act_sel_12),
        ("D", "Select Exercise 13", act_sel_13),
        ("E", "Select Exercise 14", act_sel_14),
        ("F", "Select Exercise 15", act_sel_15),
        ("o", "Open PDF (Browser)", act_open_pdf),
        ("v", "Open PDF Viewer", act_open_pdf_viewer),
        ("b", "Back", act_back),
    ]
    prompt_string = "Select Exercise [" + magenta("1") + "-" + magenta("9") + ", " + magenta("A") + "-" + \
        magenta("F") + "]  Open PDF (Browser) [" + magenta("o") + "]  Open PDF Viewer [" + \
        magenta("v") + "]  Back [" + magenta("b") + "]"

    while True:
        message = selected_group.group_string(state) + "\n\n"
        i = 1
        for e in range(state.frame.first_ex, state.frame.last_ex + 1):
            message += "\t[{}] {}\n".format(magenta(hex(i)[2:].upper()), sheet.exercises[e - 1].short_rep())
            i += 1
        state.sel_method = act_exercise_view
        act = handle_action(state, actions, message, prompt_string=prompt_string)
        if act == "b":
            state.frame.autosave()
            break

    state.sel_method = None
    state.selected_group = None


def act_sanity_check(state):
    points = sc_points()
    sane = True
    for e in state.frame.data.values():
        for ex in e.sheet.exercises:
            for a in ex.answers:
                if a.deductions.deducted_points() < -1 * points[ex.ex_num - 1][a.num - 1]:
                    print(red("Invalid deduction found for group \"{}\" exercise {}.{}").format(e.group,
                                                                                                ex.ex_num, a.num))
                    sane = False

    if not sane:
        print()
        print(red("Invalid deductions found, see above output"))
    else:
        print(green("No invalid deductions found"))
    

def act_statistics(state):
    stat_frame = state.frame.csv_df
    groups = state.frame.data.keys()
    stat_pts = []
    for g in groups:
        if state.frame.data[g].sheet.grade_status() == GRADED:
            stat_pts.append(stat_frame[stat_frame.group == g]["points"].sum())

    mean = statistics.mean(stat_pts)
    stdev = statistics.stdev(stat_pts)
    median = statistics.median(stat_pts)
    print(bright("Statistics of graded exercises:"))
    print(f"    Mean: \t{mean:.2f}".expandtabs(16))
    print(f"    Stdev: \t{stdev:.2f}".expandtabs(16))
    print(f"    Median: \t{median:.2f}".expandtabs(16))
    print(f"    Min: \t{min(stat_pts)}".expandtabs(16))
    print(f"    Max: \t{max(stat_pts)}".expandtabs(16))


# ========== UTILITY ========== #

def suffix_string(csv_name, suffix):
    return csv_name.replace(".csv", "") + suffix + ".csv"


def print_err(msg):
    sys.stderr.write(bright(red(msg)) + "\n")


def print_info(msg):
    print(magenta("INFO: " + str(msg)))


def update_max_lens(group_len, name_len):
    global MAX_GROUP_LEN
    global MAX_NAMES_LEN
    MAX_GROUP_LEN = max(MAX_GROUP_LEN, group_len)
    MAX_NAMES_LEN = max(MAX_NAMES_LEN, name_len)


def reset_max_lens():
    global MAX_GROUP_LEN
    global MAX_NAMES_LEN
    MAX_GROUP_LEN = 0
    MAX_NAMES_LEN = 0

# Points for sanity check
def sc_points():
    points = []
    for i in range(DEFAULT_FIRSTEX, DEFAULT_LASTEX + 1):
        exc = EXERCISES.get(str(i))
        if exc is not None:
            points.append(exc)
    return points


# ========== FEEDBACK EXPORT ========== #

def get_grade(frame, group):
    if group in frame.data:
        act_pts, _ = frame.data[group].sheet.range_points()
        return act_pts


def get_feedback(frame, group):
    if group in frame.data:
        return frame.data[group].sheet.html_string()


def write_grades(frame: Frame, grade_file: str):
    ggf = "graded-"+ grade_file
    print(bright(f"Exporting Moodle grades to {ggf}....."), end="")

    if not os.path.exists(grade_file):
        print(red(f"Grade file {grade_file} not found, exiting"))
        exit(1)

    gf = pd.read_csv(grade_file)
    for _, student in gf.iterrows():
        if student["Group"] in frame.data:
            _, max_pts = frame.data[student["Group"]].sheet.range_points()
            if max_pts != student["Maximum Grade"]:
                print(red(f"maximum points in exercise range ({max_pts}) do not correspond to max points in grade file ({student['Maximum Grade']}), exiting"))
                exit(1)
        
    gf["Grade"] = [get_grade(frame, d["Group"]) for _,d in gf.iterrows()]
    gf["Feedback comments"] = [get_feedback(frame, d["Group"]) for _,d in gf.iterrows()]
    now = dt.datetime.now().strftime("%A, %d %b %Y, %I:%M %p")
    gf["Last modified (grade)"] = [now if get_grade(frame, d["Group"]) is not None else "-" for _,d in gf.iterrows()]
    
    gf.to_csv(ggf, index=None)
    print(bright("done"))
            
        

# ========== MAIN ========== #

def main():
    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--exercises', default="{}-{}".format(DEFAULT_FIRSTEX, DEFAULT_LASTEX),
                        help="Range for exercises")
    parser.add_argument('-s', '--suffix', help="File suffix for considered csv files")
    parser.add_argument('-c', '--config', default="config.json", help="Config file for the sheet data")
    parser.add_argument('-gf', '--grades-file', help="Moodle grades file to write output to")
    parser.add_argument('--no-color', action="store_false", help="Turn off colored terminal output")
    parser.add_argument('--version', action="store_true", help="Display version and license information")
    args = parser.parse_args()

    # set COLOR according to argument
    global COLOR
    COLOR = args.no_color

    # display version if specified
    if args.version:
        print_version()
        exit(0)

    # parse exercise range
    m = re.search(EX_PATTERN, args.exercises)
    first_ex = m.group(1)
    last_ex = m.group(2)

    if int(first_ex) < 1 or int(last_ex) > 15:
        print_err("Exercises must be between 1 and 15, exiting")
        exit(1)

    # check whether json file exists
    if not os.path.exists(args.config):
        print_err("No configuration file found at path \"{}\", exiting".format(args.config))
        exit(1)

    # parse json and update globals

    global CSV_BASENAME
    global CSV_PATTERN
    global AUTOSAVE_FILENAME
    global DEDUCTION_FILE
    global SHEETNAME
    global EXERCISES
    global PDF_PATH
    global GROUP_SIZE

    sheet_config = json.loads(open(args.config).read())  # load json
    SHEETNAME = sheet_config["sheetname"]  # update sheet name
    if "pdf-path" in sheet_config:
        PDF_PATH = sheet_config["pdf-path"] 
    if "group-size" in sheet_config:
        GROUP_SIZE = int(sheet_config["group-size"])

    CSV_BASENAME = SHEETNAME + CSV_BASENAME
    CSV_PATTERN = SHEETNAME + CSV_PATTERN
    AUTOSAVE_FILENAME = AUTOSAVE_FILENAME.format(SHEETNAME)
    DEDUCTION_FILE = DEDUCTION_FILE.format(SHEETNAME)

    first_conf_ex = DEFAULT_FIRSTEX 

    for i in range(DEFAULT_FIRSTEX, DEFAULT_LASTEX + 1):
        if sheet_config.get("exercises").get(str(i)) is not None:
            break
        first_conf_ex = i+1
    
    exercises = {}
    for i in range(first_conf_ex, DEFAULT_LASTEX + 1):
        ex_points = sheet_config.get("exercises").get(str(i))
        if ex_points is None:  # break at first non-encountered exercise
            break
        exercises[str(i)] = ex_points
    EXERCISES = exercises

    # adapt suffix
    if args.suffix is not None:
        AUTOSAVE_FILENAME = suffix_string(AUTOSAVE_FILENAME, args.suffix)
        CSV_BASENAME = suffix_string(CSV_BASENAME, args.suffix)
        CSV_PATTERN = suffix_string(CSV_PATTERN, args.suffix)

    # Create Frame
    f = Frame(first_ex, last_ex)

    f.autosave()

    if args.grades_file:
        write_grades(f, args.grades_file)
    else:
        # start main menu
        menu(f)


if __name__ == "__main__":
    main()
